package com.devcamp.order.model;

import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;
@Entity
@Table(name="customers")
public class CCustomer {
	public CCustomer() {

	}

	public CCustomer(String fullName, String email, String phone, String address) {
		this.fullName = fullName;
		this.email = email;
		this.phone = phone;
		this.address = address;
	}

	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name="full_name")
	private String fullName;
	
	@Column(name="email")
	private String email;
	
	@Column(name="phone")
	private String phone;
	
	@Column(name="address")
	private String address;
	
	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    @JsonManagedReference
	private Set<COrder> orders;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Set<COrder> getOders() {
		return orders;
	}

	public void setOders(Set<COrder> oders) {
		this.orders = oders;
	}
}
